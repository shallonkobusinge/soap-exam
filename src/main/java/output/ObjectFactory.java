
package output;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the output package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: output
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateItemDetailsRequest }
     * 
     */
    public CreateItemDetailsRequest createCreateItemDetailsRequest() {
        return new CreateItemDetailsRequest();
    }

    /**
     * Create an instance of {@link ItemDetails }
     * 
     */
    public ItemDetails createItemDetails() {
        return new ItemDetails();
    }

    /**
     * Create an instance of {@link GetItemDetailsRequest }
     * 
     */
    public GetItemDetailsRequest createGetItemDetailsRequest() {
        return new GetItemDetailsRequest();
    }

    /**
     * Create an instance of {@link UpdateItemDetailsRequest }
     * 
     */
    public UpdateItemDetailsRequest createUpdateItemDetailsRequest() {
        return new UpdateItemDetailsRequest();
    }

    /**
     * Create an instance of {@link GetAllItemDetailsRequest }
     * 
     */
    public GetAllItemDetailsRequest createGetAllItemDetailsRequest() {
        return new GetAllItemDetailsRequest();
    }

    /**
     * Create an instance of {@link GetAllItemDetailsResponse }
     * 
     */
    public GetAllItemDetailsResponse createGetAllItemDetailsResponse() {
        return new GetAllItemDetailsResponse();
    }

    /**
     * Create an instance of {@link UpdateItemDetailsResponse }
     * 
     */
    public UpdateItemDetailsResponse createUpdateItemDetailsResponse() {
        return new UpdateItemDetailsResponse();
    }

    /**
     * Create an instance of {@link DeleteItemDetailsRequest }
     * 
     */
    public DeleteItemDetailsRequest createDeleteItemDetailsRequest() {
        return new DeleteItemDetailsRequest();
    }

    /**
     * Create an instance of {@link GetItemDetailsResponse }
     * 
     */
    public GetItemDetailsResponse createGetItemDetailsResponse() {
        return new GetItemDetailsResponse();
    }

    /**
     * Create an instance of {@link CreateItemDetailsResponse }
     * 
     */
    public CreateItemDetailsResponse createCreateItemDetailsResponse() {
        return new CreateItemDetailsResponse();
    }

    /**
     * Create an instance of {@link DeleteItemDetailsResponse }
     * 
     */
    public DeleteItemDetailsResponse createDeleteItemDetailsResponse() {
        return new DeleteItemDetailsResponse();
    }

}

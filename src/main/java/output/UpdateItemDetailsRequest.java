
package output;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="itemDetails" type="{http://soap/items}ItemDetails"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itemDetails"
})
@XmlRootElement(name = "UpdateItemDetailsRequest", namespace = "http://soap/items")
public class UpdateItemDetailsRequest {

    @XmlElement(namespace = "http://soap/items", required = true)
    protected ItemDetails itemDetails;

    /**
     * Gets the value of the itemDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ItemDetails }
     *     
     */
    public ItemDetails getItemDetails() {
        return itemDetails;
    }

    /**
     * Sets the value of the itemDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemDetails }
     *     
     */
    public void setItemDetails(ItemDetails value) {
        this.itemDetails = value;
    }

}

package com.exam.soap.stock.services;

import com.exam.soap.stock.bean.Item;

import java.util.List;

public interface IItemService {
    List<Item> getAllItem();
    Item getItemById(Integer itemId);
    boolean addItem(Item item);
    void updateItem(Item item);
    void deleteItem(Item item);

}

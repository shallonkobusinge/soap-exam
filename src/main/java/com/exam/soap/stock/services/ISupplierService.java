package com.exam.soap.stock.services;

import com.exam.soap.stock.bean.Supplier;

import java.util.List;

public interface ISupplierService {
    List<Supplier> getAllSuppliers();
    Supplier getSupplierById(Integer supplierId);
    boolean addSupplier(Supplier student);
    void updateSupplier(Supplier student);
    void deleteSupplier(Supplier student);

}

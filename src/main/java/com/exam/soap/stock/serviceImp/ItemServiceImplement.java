package com.exam.soap.stock.serviceImp;

import com.exam.soap.stock.bean.Item;
import com.exam.soap.stock.services.IItemService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemServiceImplement implements IItemService {
    @Override
    public List<Item> getAllItem() {
        return null;
    }

    @Override
    public Item getItemById(Integer itemId) {
        return null;
    }

    @Override
    public boolean addItem(Item item) {
        return false;
    }

    @Override
    public void updateItem(Item item) {

    }

    @Override
    public void deleteItem(Item item) {

    }
}

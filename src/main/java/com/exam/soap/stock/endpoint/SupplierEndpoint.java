package com.exam.soap.stock.endpoint;

import com.exam.soap.stock.bean.Item;
import com.exam.soap.stock.bean.Supplier;
import com.exam.soap.stock.repository.SupplierRepository;
import com.exam.soap.stock.serviceImp.SupplierServiceImplement;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import output.*;

import java.util.List;

@Endpoint
public class SupplierEndpoint {

    @Autowired
    private SupplierRepository supplierRepository;

    @Autowired
    private SupplierServiceImplement serviceImplement;

    @PayloadRoot(namespace = "http://soap/suppliers", localPart = "GetSupplierRequest")
    @ResponsePayload
    public GetSupplierResponse getSupplier(@RequestPayload GetSupplierRequest request) {
        GetSupplierResponse response = new GetSupplierResponse();
        SupplierInfo supplierInfo = new SupplierInfo();
        BeanUtils.copyProperties(serviceImplement.getSupplierById(request.getId()), supplierInfo);
        response.setSupplierInfos(supplierInfo);
        return response;
    }

    @PayloadRoot(namespace = "http://soap/suppliers", localPart = "GetAllSuppliersRequest")
    @ResponsePayload

    public GetAllSuppliersResponse getAllSuppliers() {
        GetAllSuppliersResponse allSuppliersResponse = new GetAllSuppliersResponse();

        List<Supplier> suppliers = serviceImplement.getAllSuppliers();
        for(Supplier supplier: suppliers){
            GetSupplierResponse supplierResponse = mapSupplierInfo(supplier);
            allSuppliersResponse.getSupplierInfo().add(supplierResponse.getSupplierInfos());
        }
        return allSuppliersResponse;
    }

    private GetSupplierResponse mapSupplierInfo(Supplier supplier) {
        SupplierInfo supplierInfo = mapSupplier(supplier);

        GetSupplierResponse supplierResponse = new GetSupplierResponse();

        supplierResponse.setSupplierInfos(supplierInfo);
        return supplierResponse;
    }

    private SupplierInfo mapSupplier(Supplier supplier) {
        SupplierInfo supplierInfo = new SupplierInfo();
        supplierInfo.setSupplierId(supplier.getId());
        supplierInfo.setNames(supplier.getNames());
        supplierInfo.setEmail(supplier.getEmail());
        supplierInfo.setMobile(supplier.getMobile());
        return supplierInfo;
    }

    @PayloadRoot(namespace = "http://soap/suppliers", localPart = "addSupplierRequest")
    @ResponsePayload
    public AddSupplierResponse addSupplier(@RequestPayload AddSupplierRequest request) {
        AddSupplierResponse response = new AddSupplierResponse();
        ServiceStatus serviceStatus = new ServiceStatus();
        Supplier supplier = new Supplier();
        supplier.setEmail(request.getEmail());
        supplier.setNames(request.getNames());
        supplier.setMobile(request.getMobile());

        boolean flag = serviceImplement.addSupplier(supplier);
        if (flag == false) {
            serviceStatus.setMessage("Supplier Already Available");
            response.setServiceStatus(serviceStatus);
        } else {
            SupplierInfo supplierInfo = new SupplierInfo();
            BeanUtils.copyProperties(supplier,supplierInfo);
            response.setSupplierInfo(supplierInfo);
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Supplier Added Successfully");
            response.setServiceStatus(serviceStatus);
        }
        return response;
    }

    @PayloadRoot(namespace = "http://soap/suppliers", localPart = "updateSupplierRequest")
    @ResponsePayload
    public UpdateSupplierResponse updateSupplier(@RequestPayload UpdateSupplierRequest request) {
        ServiceStatus serviceStatus = new ServiceStatus();
        serviceStatus.setStatusCode("SUCCESS");
        serviceStatus.setMessage("Content Updated Successfully");
        UpdateSupplierResponse response = new UpdateSupplierResponse();
        response.setServiceStatus(serviceStatus);
        Supplier supplier = supplierRepository.findSupplierById(request.getSupplierInfo().getSupplierId());
        supplier.setEmail(request.getSupplierInfo().getEmail());
        supplier.setNames(request.getSupplierInfo().getNames());
        supplier.setMobile(request.getSupplierInfo().getMobile());
        supplierRepository.save(supplier);
                return response;
    }



    @PayloadRoot(namespace = "http://soap/suppliers", localPart = "deleteSupplierRequest")
    @ResponsePayload
    public DeleteSupplierResponse deleteSupplier(@RequestPayload DeleteSupplierRequest request) {
        Supplier supplier = serviceImplement.getSupplierById(request.getSupplierId());
        ServiceStatus serviceStatus = new ServiceStatus();
        if (supplier == null ) {
            serviceStatus.setStatusCode("FAIL");
            serviceStatus.setMessage("Content Not Available");
        } else {
            serviceImplement.deleteSupplier(supplier);
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Supplier Deleted Successfully");
        }
        DeleteSupplierResponse response = new DeleteSupplierResponse();
        response.setServiceStatus(serviceStatus);
        return response;
    }
}

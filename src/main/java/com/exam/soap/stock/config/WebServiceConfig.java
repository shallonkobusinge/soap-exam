package com.exam.soap.stock.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;


@EnableWs

@Configuration
public class WebServiceConfig{

		@Bean
		public ServletRegistrationBean messageDispatcherServlet(ApplicationContext context) {
			MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
			messageDispatcherServlet.setApplicationContext(context);
			messageDispatcherServlet.setTransformWsdlLocations(true);
			return new ServletRegistrationBean(messageDispatcherServlet, "/ws/shallon/stocks/*");
		}

		@Bean(name = "item")
		public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema itemSchema) {
			DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
			definition.setPortTypeName("StudentPort");
			definition.setTargetNamespace("http://soap/items");
			definition.setLocationUri("/ws/shallon/stocks");
			definition.setSchema(itemSchema);
			return definition;
		}

	@Bean(name = "supplier")
	public DefaultWsdl11Definition supplierWsdlDefinition(XsdSchema supplierSchema) {
		DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
		definition.setPortTypeName("StudentPort");
		definition.setTargetNamespace("http://soap/suppliers");
		definition.setLocationUri("/ws/shallon");
		definition.setSchema(supplierSchema);
		return definition;
	}
		@Bean
		public XsdSchema itemSchema() {
			return new SimpleXsdSchema(new ClassPathResource("item-details.xsd"));
		}

	@Bean
	public XsdSchema supplierSchema() {
		return new SimpleXsdSchema(new ClassPathResource("supplier-details.xsd"));
	}

}
